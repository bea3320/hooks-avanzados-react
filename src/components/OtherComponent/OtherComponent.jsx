import { useContext, useState } from "react";
import { UserContext } from "../../App";

const OtherComponent = () => {
  const user = useContext(UserContext);
  const [data, setData] = useState({});

  const handleInput = (event) => {
    const { name, value } = event.target;

    setData({ ...data, [name]: value });
  };

  const submitForm = (event) => {
    event.preventDefault();
    user.handleUser(data);
    setData({});
  };

  return (
    <div>
      <div>Cambiar de usuario</div>
      <h1>Nombre de usuario: {user.name}</h1>
      <h1>Email de usuario: {user.email}</h1>
      <h1>Cole de usuario: {user.school}</h1>
      <form onSubmit={submitForm}>
        <input
          type="text"
          onChange={handleInput}
          name="name"
          placeholder="Nombre"
        />
        <input
          type="email"
          onChange={handleInput}
          name="email"
          placeholder="Email"
        />
        <input
          type="company"
          onChange={handleInput}
          name="school"
          placeholder="Colegio"
        />
        <button type="submit">Enviar</button>
      </form>
    </div>
  );
};
export default OtherComponent;
