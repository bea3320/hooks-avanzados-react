import { useEffect, useContext } from "react";
import {UserContext} from '../../App';
import "./Home.scss";

const Home = () => {
  const user = useContext(UserContext);

  useEffect(() => {
    const scrollHandler = () => {
      console.log(`Scrolling at: ${window.scrollY}`);
    };
    document.addEventListener("scroll", scrollHandler);
    /* console.log("Actuo de componentDidMount"); */

    //componentWillUnmount
    return () => {
      console.log("Voy a desmontar el componente Home");
      document.removeEventListener("scroll", scrollHandler);
    };
    /* Si le paso una variable dentro del array vacío de useEffect y 
    esta variable cambia de valor, se ejecuta de nuevo el usseEffect */
  }, []);

  return (
  <div className="home">
  <div>Esto es Home, nombre del usuario: {user.name}</div>
  </div>
  );
};

export default Home;
