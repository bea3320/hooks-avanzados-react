import { useState, createContext } from "react";
import "./App.scss";
import { Counter, Home, RickYMorty } from "./components";

const INITIAL_STATE = {
  name: "Mario",
  email: "mario@gmail.com",
  school: "Juan Gris",
};

export const UserContext = createContext(null);

const App = () => {
  const [isOpen, setIsOpen] = useState(false);
  const [user, setUser] = useState(INITIAL_STATE);
  const handleUser = (newUser) => {
    setUser({ ...user, ...newUser });
  };

  return (
    <UserContext.Provider value={{ ...user, handleUser }}>
      <div className="app">
        <div>HOOKS AVANZADOS</div>
        <div>
          <button onClick={() => setIsOpen(!isOpen)}>
            Montar componente Home
          </button>
          {isOpen && <Home />}
        </div>
        <Counter />
        <RickYMorty />
      </div>
    </UserContext.Provider>
  );
};

export default App;
